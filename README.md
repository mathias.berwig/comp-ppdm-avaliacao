# Programação para Dispositivos Móveis - Avaliação 1

## Objetivos

Desenvolver um aplicativo Android em que o usuário possa jogar um ou dois dados - ele deve poder escolher - de cores diferentes e o resultado seja apresentado na tela.

## Tecnologias Utilizadas

- Java 8
- AndroidX AppCompat, ConstraintLayout
- Google Material Design

## Resultado

![Sample Video](media/capture.webm)