package io.github.mathiasberwig.avaliacao;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;

import java.util.Objects;

import androidx.annotation.ColorRes;
import androidx.annotation.DrawableRes;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;

public class MainActivity extends AppCompatActivity {

    private ImageView j1i1, j1i2, j2i1, j2i2;
    private boolean jogarDoisDados = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Localiza as referências de Views na GUI
        j1i1      = findViewById(R.id.jogo_1_img_1);
        j1i2      = findViewById(R.id.jogo_1_img_2);
        j2i1      = findViewById(R.id.jogo_2_img_1);
        j2i2      = findViewById(R.id.jogo_2_img_2);

        // Define a distância da câmera para as imagens a fim de evitar bugs na animação
        final float escala = getResources().getDisplayMetrics().density;
        final float cameraDistance = 8000 * escala;
        for (ImageView v : new ImageView[]{j1i1, j1i2, j2i1, j2i2}) {
            v.setCameraDistance(cameraDistance);
        }

        // Define onClick do botão Jogar
        findViewById(R.id.btn_jogar).setOnClickListener(this::onJogarClick);

        // Define onCheckedListener do Switch
        SwitchCompat switchDoisLados = findViewById(R.id.switch_dois_dados);
        switchDoisLados.setOnCheckedChangeListener(this::onAlterarNumeroDados);
    }

    private void onAlterarNumeroDados(CompoundButton compoundButton, boolean checked) {
        j2i1.setVisibility(checked ? View.VISIBLE : View.INVISIBLE);
        j2i2.setVisibility(checked ? View.VISIBLE : View.INVISIBLE);
        jogarDoisDados = checked;
    }

    private void onJogarClick(View v) {
        jogarDado(j1i1, j1i2);

        if (jogarDoisDados) jogarDado(j2i1, j2i2);
    }

    /**
     * Realiza a animação das imagens de dados.
     * @param image1 Imagem 1 do jogo.
     * @param image2 Imagem 2 do jogo.
     */
    private void jogarDado(final ImageView image1, final ImageView image2) {
        final ImageView visibleView = image1.getAlpha() == 1f ? image1 : image2;
        final ImageView hiddenView  = visibleView == image1 ? image2 : image1;

        // Define o ícone da ImageView que será mostrada a partir do número sorteado
        hiddenView.setImageDrawable(getDadoColorido(jogarDado()));

        // Anima e define a imagem do dado de acordo com o número sorteado
        rotacionar(visibleView, hiddenView);
    }

    /**
     * Anima a ImagemVisível para rotacionar 180º no eixo X durante 600ms à medida que desaparece;
     * ao término da animação, inicia outra para a view que está oculta, mostrando-a e rotacionando-a
     * menos 180º.
     */
    private void rotacionar(final ImageView visibleView, final ImageView hiddenView) {
        visibleView.animate()
                .alpha(0f)
                .rotationXBy(180)
                .setDuration(600)
                .withEndAction(() ->
                        hiddenView
                                .animate()
                                .alpha(1f)
                                .rotationXBy(-180)
                                .setDuration(600));
    }

    /**
     * @param numero número entre 1 e 6.
     * @return imagem do dado numerado com uma cor pré-definida.
     */
    private Drawable getDadoColorido(int numero) {
        if (numero < 1 || numero > 6) {
            throw new IllegalArgumentException("Número inválido");
        }

        // Obtém o Drawable do dado a partir de seu número
        final Drawable imagem = Objects.requireNonNull(getDrawable(getImagemDado(numero))).mutate();

        // Tonaliza a imagem de acordo com o número
        imagem.setTint(getColor(getCorDado(numero)));

        return imagem;
    }

    /**
     * @param numero número entre 1 e 6.
     * @return identificador de recurso de cor para o número informado.
     */
    @ColorRes
    private int getCorDado(int numero) {
        switch (numero) {
            case 1: return R.color.cor_1;
            case 2: return R.color.cor_2;
            case 3: return R.color.cor_3;
            case 4: return R.color.cor_4;
            case 5: return R.color.cor_5;
            case 6: return R.color.cor_6;
            default: throw new IllegalArgumentException("Número inválido");
        }
    }

    /**
     * @param numero número entre 1 e 6.
     * @return identificador do recurso de imagem para o número informado.
     */
    @DrawableRes
    private int getImagemDado(int numero) {
        switch (numero) {
            case 1: return R.drawable.dado_1;
            case 2: return R.drawable.dado_2;
            case 3: return R.drawable.dado_3;
            case 4: return R.drawable.dado_4;
            case 5: return R.drawable.dado_5;
            case 6: return R.drawable.dado_6;
            default: throw new IllegalArgumentException("Número inválido");
        }
    }

    /**
     * @return número aleatório entre 1 e 6.
     */
    private static int jogarDado() {
        return (int) (Math.random() * 6 + 1);
    }
}
